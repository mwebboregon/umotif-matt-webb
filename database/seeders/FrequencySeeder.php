<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class FrequencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('frequencies')->insert([
            [
                'name' => 'Monthly',
                'slug' => 'monthly',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            [
                'name' => 'Weekly',
                'slug' => 'weekly',
                'created_at' => now(),
                'updated_at' => now(),
            ],

            [
                'name' => 'Daily',
                'slug' => 'daily',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
