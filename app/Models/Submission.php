<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Submission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'frequency_id',
        'cohort',
        'name',
        'birthdate',
        'daily_frequencies',
    ];

    /**
     * Get the attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'birthdate' => 'date',
        ];
    }

    /**
     * Returns the associated frequency with a given questionnaire
     */
    public function frequency(): BelongsTo
    {
        return $this->belongsTo(Frequency::class);
    }
}
