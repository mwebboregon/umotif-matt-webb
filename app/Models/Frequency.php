<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Frequency extends Model
{
    /**
     * Returns associated submissions based off of a given frequency ID
     */
    public function submission(): HasMany
    {
        return $this->hasMany(Submission::class);
    }
}
