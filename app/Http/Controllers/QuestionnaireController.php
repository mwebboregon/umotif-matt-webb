<?php

namespace App\Http\Controllers;

use App\Models\Frequency;
use App\Models\Submission;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QuestionnaireController extends Controller
{
    /**
     * Displays the questionnaire
     */
    public function index(): View
    {
        $data['frequencies'] = Frequency::query()->orderBy('id', 'desc')->get();
        return view('index', $data);
    }

    /**
     * Accepts and processes applicant form submissions
     */
    public function store(Request $request): RedirectResponse
    {
        $messages = [
            'name.required' => 'Your first name is required.',
            'frequency.required' => 'Migraine frequency is required.',
            'birthdate.required' => 'Your date of birth is required.',
        ];

        $request->validate([
            'name' => 'bail|required|max:18',
            'frequency' => 'required',
            'daily_frequency' => 'nullable|max:2',
            'birthdate' => 'required',
        ], $messages);

        // Validate the birth date
        $givenDate = Carbon::createFromFormat('Y-m-d', $request->input('birthdate'));
        $targetBirthDate = Carbon::now()->subYears(18)->format('Y-m-d');

        $dateCheck = ($givenDate < $targetBirthDate) ? true : false;

        if (!$dateCheck) {
            return redirect()->back()->withErrors(['msg' => 'Partcipants must be over 18 years of age']);
        }

        // Verify that the daily migration frequencies are more than 0
        if ($request->filled('daily_frequency') && $request->input('daily_frequency') == '0') {
            return redirect()->back()->withErrors(['msg' => 'Please select daily frequency total.']);
        }

        // Establish a new record for this submission
        $designatedCohort = $request->filled('daily_frequency') ? 'B' : 'A';
        $frequency = Frequency::where('slug', $request->input('frequency'))->first();
        $dailyFrequency = $request->filled('daily_frequency') ? $request->input('daily_frequency') : null;

        $submission = Submission::create([
            'frequency_id' => $frequency->id,
            'cohort' => $designatedCohort,
            'name' => $request->input('name'),
            'birthdate' => $givenDate,
            'daily_frequencies' => $dailyFrequency,
        ]);

        // Show the summary
        return redirect()->route('guest.results', ['id' => $submission->id]);
    }

    /**
     * Displays a summary of their submission
     */
    public function show(string $id): View
    {
        $data['results'] = Submission::find(intval($id));
        return view('results', $data);
    }
}
