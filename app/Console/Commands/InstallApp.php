<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class InstallApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'umotif:installer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'instantiates the migrations and the related database seeders';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        if ($this->confirm('Do you wish to start the installer?', true)) {
            $this->info('Starting the installer...');
            $this->newLine(3);
            $this->info('Running the migrations...');

            $migrations = $this->withProgressBar(Artisan::call('migrate'), function () {
                $this->info('In progress!');
            });
            $this->newLine(3);

            $seeders = $this->withProgressBar(Artisan::call('db:seed'), function () {
                $this->info('Populating the necessary tables!');
            });
            $this->newLine(3);

            $this->info('All done!');
        } else {
            $this->error('Please go back and answer with Yes...');
        }
    }
}
