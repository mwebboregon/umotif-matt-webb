import "./bootstrap";

import { createApp } from "vue";
import components from "/resources/js/components/index.js";

const app = createApp({
  mounted() {},
});

components(app);
app.mount("#app");
