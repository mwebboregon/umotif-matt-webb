import SubmissionForm from './SubmissionForm.vue';

export default (app) => {
	app.component('submissionform', SubmissionForm);
};
