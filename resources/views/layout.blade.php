<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head prefix="og: https://ogp.me/ns#">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Umotif | Matt Webb Dev Test</title>
        <meta name="description" content="This dev test was done by Matt Webb. My last name is not indicative to my profession. It's just a coincidence.">
        <meta property="og:locale" content="en_US">
        <meta property="og:site_name" content="Umotif - Dev Test">
        @vite('resources/css/app.css')
        @vite('resources/js/app.js')
        <link rel="icon" href="https://umotif.com/wp-content/themes/umotif/img/favicon.svg" type="image/svg+xml">
    </head>
    <body>
    <x-header />
    
    <div>
        <!-- Start content -->

        @yield('content')

        <!-- End content -->
    
    </div>

    <x-footer />

    @stack('scripts')

    </body>
</html>    