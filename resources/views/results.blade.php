@extends('layout')
@section('content')
<section class="hero is-primary mb-6">
    <div class="hero-body">
      <p class="title pb-2">Thank You!</p>
      <p class="subtitle">We have received your submitted information and will reach out to you in the near future. Here is a summary of your information.</p>
    </div>
  </section>
<section>
    <main class="container is-fluid">
        <div class="card">
            <div class="card-content">
                <div class="content">
                    <h1 class="is-size-2">Hello {{ ucfirst($results->name) }}!</h1>
                    <hr>
                    <p class="is-size-4 has-text-weight-semibold pb-4">Summary of Your Information</p>
                    <ul>
                        <li>Your date of birth is <u>{{ $results->birthdate->todatestring() }}</u></li>
                        <li>You are experiencing <u>{{ strtolower($results->frequency->name) }}</u> migraines.</li>
                        @if(!empty($results->daily_frequencies))
                            <li>You are experiencing <u>{{ $results->daily_frequencies }} migraines per day</u>.</li>
                        @endif
                        <li>Based on the frequency of your migraines, you would be part of <u>Cohort {{ $results->cohort }}.</u></li>
                      </ul>
                </div>
            </div>
        </div>
    </main>
</section>
@endsection