<footer class="footer has-text-centered">
    
    <a href="https://www.linkedin.com/company/umotif" class="mg-x-sm" title="uMotif on LinkedIn">
        <svg aria-hidden="true" viewBox="0 0 24 24" width="1.4em" height="1.4em">
        <path d="M6.94 5a2 2 0 1 1-4-.002 2 2 0 0 1 4 .002zM7 8.48H3V21h4V8.48zm6.32 0H9.34V21h3.94v-6.57c0-3.66 4.77-4 4.77 0V21H22v-7.93c0-6.17-7.06-5.94-8.72-2.91l.04-1.68z"/>
        </svg>
    </a>
        <a href="https://twitter.com/uMotif" class="mg-x-sm" title="uMotif on X">
        <svg aria-hidden="true" viewBox="0 0 24 24" width="1.4em" height="1.4em">
        <path d="M18.901 1.153h3.68l-8.04 9.19L24 22.846h-7.406l-5.8-7.584-6.638 7.584H.474l8.6-9.83L0 1.154h7.594l5.243 6.932ZM17.61 20.644h2.039L6.486 3.24H4.298Z"/>
        </svg>
    </a>
    <p>&copy; uMotif Limited 2012 - 2024</p>
<address>3rd Floor, RE-DEFINED, 30 Stamford St, London, SE1 9LQ</address>
<p class="mg-b-lg">Company incorporated in England number: 07993440</p>
    <p>
    <a href="/privacypolicy/">Privacy Policy</a>
</p>
        <p>
    <a href="/cookie-policy">Cookie Policy</a>
</p>
        <p>
    <a href="/anti-bribery/">Anti-Bribery Policy</a>
</p>
        <p>
    <a href="/anti-slavery/">Anti-Slavery Policy</a>
</p>
    </footer>	