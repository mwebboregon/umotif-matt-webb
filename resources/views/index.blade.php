@extends('layout')
@section('content')
<section class="hero is-large herobg mb-6 is-hidden-touch">
    <div class="hero-body">
        <div class="columns is-mobile">
            <div class="column is-one-third-desktop is-half-tablet is-fullwidth-mobile">
                <div class="card bg-motif-blue has-shadow is-spaced">
                    <div class="card-content ">
                        <div class="content">                                         
                            <p class="title has-text-white pb-4">Migraines - Clinical Trial Registration</p>
                            <p class="subtitle has-text-white pb-2">Thank you for your interest in this clinical trial, we look forward to working with you. Participants must be a minimum of <strong class="has-text-white">18 years of age</strong> in order to participate.</p>
                            <p class="subtitle has-text-white">Please be sure to fill in the form in its entirety to gain instant results to your eligibility for participation.</p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>        
    </div>
</section>

<section class="hero is-small is-info-dark mb-6 is-hidden-desktop" >
    <div class="hero-body">
        <div class="columns is-mobile">
            <div class="column is-fullwidth">
                <div class="card bg-motif-blue has-shadow is-spaced">
                    <div class="card-content ">
                        <div class="content">                                         
                            <p class="title has-text-white pb-4">Clinical Trial Eligibility Questionnaire</p>
                            <p class="subtitle has-text-white">Thank you for your interest in this clinical trial, we look forward to working with you. Please be sure to fill in the form in its entirety to gain instant results to your eligibility for participation.</p>
                        </div>
                    </div>
                </div>
            </div>            
        </div>        
    </div>
</section>
<section>
    <main class="container is-fluid">       

        <form method="POST" action="{{ route('guest.store') }}" class="mb-8 container is-max-desktop" enctype="application/x-www-form-urlencoded">
            @csrf

            <div id="app">
                @if ($errors->any())
                    <div class="has-background-danger-dark p-4">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="has-text-white-bis is-size-6">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <submissionform :frequencies="{{ json_encode($frequencies) }}"></submissionform>
            </div>            
        </form>
    </main>
</section>
@endsection