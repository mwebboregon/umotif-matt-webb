<?php

use App\Http\Controllers\QuestionnaireController;
use Illuminate\Support\Facades\Route;

Route::name('guest.')->group(function () {
    Route::controller(QuestionnaireController::class)->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/', 'store')->name('store');
        Route::get('results/{id}', 'show')->name('results');
    });
});
