
## My Dev Environment
- PHP v8.3.6 (8.2 is the minimum requirement) 
- Laravel v11.3.1
- Vue v3.4.21
- Bulma CSS v1.0.0
- SQLite
- Yarn v1.22.22

## Installation

#### 1) Clone this repo 
```sh
git clone git@gitlab.com:mwebboregon/umotif-matt-webb.git  
```

#### 2) Run the following from your terminal:
```sh
cd umotif-matt-webb
composer install
```

#### 3) I have included an installer which handles the migrations and a seeder for the frequencies table
```sh
php artisan umotif:installer
```
#### 4) It's a production ready build. 
All that's left is to go check it out.

## Thanks!